# Jest Workshops

## Workshops Goal

This Jest workshops intends to help developers learning unit testing React components at Cdiscount with [Jest](https://facebook.github.io/jest/) and its own framework built on it.

## Your Goal

The `exercises` folder contains a set of failing tests. For each test, you will need to write code using Jest or Cdiscount Jest framework. Once all of your tests pass on an exercise, you will want to move onto the next one.

## Requirements

* [Git](https://git-scm.com/) or [Git for Windows](https://gitforwindows.org/)
* [Node.js](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/lang/en/)

## Getting Started

### Install dependencies

```shell
yarn
```

### How To Run Tests

To run all the test suite:

```shell
yarn test
```

To run a specific test file:

```shell
yarn test -- exercises/01-intro.js
```

To run the updated tests as you make changes:

```shell
yarn test:watch
```

## Code of Conduct

[Code of Conduct](./CODE_OF_CONDUCT.md)

## Contributing

[Contributing](./CONTRIBUTING.md)

### How to add workshops

To add worshop:

```shell
$ hygen exercise add
? Exercise name? modules
? Exercise number? 02

Loaded templates: _templates
       added: exercises/02-modules.js
```

## License

[MIT License](./LICENSE.md)
